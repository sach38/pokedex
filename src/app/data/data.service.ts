import {Injectable} from '@angular/core';
// @ts-ignore
import pokemons from '../../assets/json/pokemons.json';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  pokemonsList: Array<Pokemon> = new Array<Pokemon>();

  constructor() {
    pokemons.forEach((pokemon: Pokemon) => {
      if (pokemon.id.toString().length <= 3) {
        this.pokemonsList.push(pokemon);
      }
    });
  }

  getPokemons() {
    return this.pokemonsList;
  }

}

export interface Pokemon {
  id: number;
  height: number;
  types: Array<string>;
  name: string;
  names: {
    fr: string;
    en: string
  };
  generation: string;
  genus: { fr: string; en: string };
  color: string;
}
