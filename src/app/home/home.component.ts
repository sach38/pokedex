import {Component, OnInit} from '@angular/core';
import {DataService, Pokemon} from '../data/data.service';
import {Data} from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  typeFrench = {
    fire: 'feu',
    grass: 'plante',
    electric: 'électrique',
    water: 'eau',
    ground: 'sol',
    rock: 'roche',
    fairy: 'fée',
    poison: 'poison',
    bug: 'insecte',
    dragon: 'dragon',
    psychic: 'psy',
    flying: 'vol',
    fighting: 'combat',
    normal: 'normal',
    dark: 'ténèbre',
    ice: 'glace',
    ghost: 'spectre',
    steel: 'acier'
  };

  colors = {
    fire: '#F39B5A',
    grass: '#78C850',
    electric: '#FADB5F',
    water: '#8EC8F3',
    ground: '#E0C068',
    rock: '#B8A038',
    fairy: '#EE99AC',
    poison: '#CD81CD',
    bug: '#A8B820',
    dragon: '#9A72FA',
    psychic: '#FA749C',
    flying: '#D3C7F7',
    fighting: '#D95048',
    normal: '#A8A878',
    dark: '#808B96',
    ice: '#98D8D8',
    ghost: '#705898',
    steel: '#B8B8D0'
  };

  pokemonList: Array<Pokemon>;
  lang: string = 'fr';

  constructor(dataService: DataService) {
    this.pokemonList = dataService.getPokemons();
  }

  ngOnInit(): void {
  }

  getTypes(types: Array<string>) {
    let r: string = '';
    if (this.lang === 'fr') {
      // @ts-ignore
      r = this.typeFrench[types[0]];
      if (types.length > 1) {
        // @ts-ignore
        r += '/' + this.typeFrench[types[1]];
      }
    } else {
      r = types[0];
      if (types.length > 1) {
        // @ts-ignore
        r += '/' + types[1];
      }
    }
    return r;
  }

  changeLang() {
    if (this.lang === 'fr') {
      this.lang = 'en';
    } else {
      this.lang = 'fr';
    }
  }

  getName(names: { fr: string; en: string }) {
    if (this.lang === 'fr') {
      return names.fr;
    } else {
      return names.en;
    }
  }

  getBackgroundColor(types: Array<string>) {
    if (types.length === 1) {
      return {'backgroundColor': this.colors[types[0]] };
    } else {
      return {'background-image': `linear-gradient(${this.colors[types[0]]}, ${this.colors[types[1]]})` };
    }
  }
  gotoDetails(id: number) {
    console.log(`id cliqué est : ${id}`)
  }
}
